import React, { Component } from 'react';
import './App.css';
import Person from './Person/Person';

class App extends Component {
  state = {
    persons: [
      {id: 'asd' ,name: 'Max', age: 25},
      {id: 'cvxb' ,name: 'Matt', age: 36},
      {id: 'tjm' ,name: 'Alejandro', age: 44}
    ],
    otherState: 'some other value',
    showPersons: false
  }

  deletePerson = (personIndex) => {
    // const persons = this.state.persons.slice(); // ES5
    const persons = [...this.state.persons]; // ES6 spread operator
    persons.splice(personIndex, 1);
    this.setState({persons: persons});
  }

  nameChangedHandler = (event,id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });

    const person = {...this.state.persons[personIndex]};
    // const person Object.assign({}, this.state.persons[personIndex]);

    person.name = event.target.value;

    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState({ persons: persons });
  }

  togglePersonHandler = () => {
    const doesShow = this.state.showPersons;
    this.setState({showPersons: !doesShow});
  }

  // MAIN
  render() {
    const styleMe = {
      backgroundColor: 'green',
      color: 'white',
      font: 'inherit',
      border: '1px solid blue',
      padding: '8px',
      cursor: 'pointer',
      boxShadow: '0 2px 3px #ccc',
    };

    let persons = null;

    if(this.state.showPersons){
      persons = (
        <div >
          {this.state.persons.map((person, index) => {
            return <Person 
            click={this.deletePerson} 
            name={person.name} 
            age={person.age}
            key={person.id} 
            changed={(event, id) => this.nameChangedHandler(event, person.id)} />
          })}
        </div>
      )
      styleMe.backgroundColor = 'red';

    }

    const classes = [];

    if(this.state.persons.length < 3){
      classes.push("red");
    }
    if(this.state.persons.length < 2){
      classes.push("bold");
    }

    return (
        <div className="App">
          <h1>Holanda</h1>
          <p class={classes.join(" ")}>Esto es su parrafo</p>
          <button 
          style={styleMe} 
          onClick={this.togglePersonHandler}>Muestra!</button>
          {persons}
        </div>
    );
  }
}

export default App;
